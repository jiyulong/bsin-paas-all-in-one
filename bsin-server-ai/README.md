# bsin-server-ai
Bsin-PaaS AI引擎后台。包括AIGC、chatGPT、wechatBot、微信公众号对接等

  **文档版本**

| 版本号 | 修改日期   | 编写   | 修改内容                     | 备注 |
| ------ | ---------- | ------ | ---------------------------- | ---- |
| V1.0.0 | 2023/04/21 | leonard | 新建                         |      |


## 相关术语

| 序号 | 术语/缩略语 | 全称和说明                                                   |
| ---- | ----------- | ------------------------------------------------------------ |
| 1    | 原子服务        | bsin-paas平台服务中不可拆分的最小服务                  |


## 架构设计

![logo](./doc/images/基于大语言模型的AI应用架构图.png)


## 向量数据库

https://github.com/milvus-io/milvus


## 视频教程
[Bsin-PaaS一分钟让公众号接入chatGPT智能客服](https://www.bilibili.com/video/BV14M4y1j7BS/?vd_source=a6a847135c9dc5b402452069e1192313)
[基于大语言模型的AI应用系统架构设计](https://www.bilibili.com/video/BV14M4y1j7BS/?vd_source=a6a847135c9dc5b402452069e1192313)