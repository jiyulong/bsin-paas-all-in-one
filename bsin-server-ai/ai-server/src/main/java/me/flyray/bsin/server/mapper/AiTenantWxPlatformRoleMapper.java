package me.flyray.bsin.server.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import me.flyray.bsin.server.domain.TenantWxPlatform;
import me.flyray.bsin.server.domain.TenantWxPlatformRole;

/**
 * @author bolei
 * @description 针对表【ai_tenant_wxmp_role】的数据库操作Mapper
 * @createDate 2023-04-25 18:41:30
 * @Entity generator.domain.AiTenantWxmpRole
 */
@Mapper
@Repository
public interface AiTenantWxPlatformRoleMapper {

    TenantWxPlatformRole selectByTenantId(String tenantId);

    void insert(TenantWxPlatformRole tenantWxmpRole);

    void deleteById(String serialNo);

    void updateById(TenantWxPlatformRole tenantWxmpRole);

}
