package me.flyray.bsin.server.mapper;


import me.flyray.bsin.server.domain.Task;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author huangzh
 * @ClassName NGDBTaskMapper
 * @DATE 2020/11/10 14:39
 */

@Repository
@Mapper
public interface TaskMapper {
    /**
     * 查询运行中的任务实例
     * @param params
     * @return
     */
    List<Task> selectTasks(Map<String, Object> params);

    /**
     * 查询历史任务实例
     * @param params
     * @return
     */
    List<Task> selectHistoricTasks(Map<String, Object> params);

    List<Task> selectTasksByUser(@Param("tenantId") String tenantId,
                                 @Param("assignee") String assignee,
                                 @Param("owner") String owner);
}
