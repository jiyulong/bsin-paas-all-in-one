package me.flyray.bsin.facade.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Path("historyProcessInst")
public interface BsinAdminHistoryProcessInstService {

    /**
     * 查询历史流程实例
     */
    @POST
    @Path("getHistoryProcessInst")
    @Produces("application/json")
    Map<String, Object> getHistoryProcessInst(Map<String, Object> requestMap);

    /**
     * 查询历史任务信息
     */
    @POST
    @Path("getHistoryActInst")
    @Produces("application/json")
    Map<String, Object> getHistoryActInst(Map<String, Object> requestMap);

    /**
     * 查询历史任务信息
     */
    @POST
    @Path("getHistoryTaskInst")
    @Produces("application/json")
    Map<String, Object> getHistoryTaskInst(Map<String, Object> requestMap);


    /**
     * 查询历史任务信息
     */
    @POST
    @Path("getApprovalRecord")
    @Produces("application/json")
    Map<String, Object> getApprovalRecord(Map<String, Object> requestMap);
}
